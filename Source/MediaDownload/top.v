module top (
	input  wire clk_12M,
	input  wire reset_n,
	input  wire flash_spi_MISO,
	output wire flash_spi_MOSI,
	output wire flash_spi_SCLK,
	output wire flash_spi_SS_n,
	input  wire uart_rxd,
	output wire uart_txd
);
	
SPIFlashBurn u0 (
	.clk_12m_clk    (clk_12M),        //   clk_12m.clk
	.reset_reset_n  (reset_n),        //     reset.reset_n
	.flash_spi_MISO (flash_spi_MISO), // flash_spi.MISO
	.flash_spi_MOSI (flash_spi_MOSI), //          .MOSI
	.flash_spi_SCLK (flash_spi_SCLK), //          .SCLK
	.flash_spi_SS_n (flash_spi_SS_n), //          .SS_n
	.data_uart_rxd  (uart_rxd),       // data_uart.rxd
	.data_uart_txd  (uart_txd)        //          .txd
);

endmodule