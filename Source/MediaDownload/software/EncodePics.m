clearvars

k = 223;
n = 255;

% for i = 0:4
%     readName = sprintf('Fishes%d.jpg', i);
%     writeName = sprintf('Fishes%d.rsenc', i);
    readName = sprintf('aquariumslide3.mp4');
    writeName = sprintf('aquariumslide3.rsenc');
    readFile = fopen(readName);

    fseek(readFile, 0,'eof');
    filelen = ftell(readFile);
    numRows = ceil(filelen/k);
    fseek(readFile, 0,'bof');
    pic = fread(readFile, [k, numRows],'uint8');
    pic = pic.';
    fclose(readFile);

    msg = gf(pic, 8);

    encoded = rsenc(msg,n,k);

    writeFile = fopen(writeName,'w');
    fwrite(writeFile, encoded.x.','uint8');
    fclose(writeFile);
% end
