/*
 * spi_flash.c
 *
 *  Created on: Nov 30, 2018
 *      Author: cgage
 */

#include <stdio.h>
#include <string.h>

#include "system.h"
#include "altera_avalon_spi.h"
#include "alt_types.h"  // alt_u32
#include "spi_flash.h"

alt_u8 Flash_BusyCheck()
{
	alt_u8 Send_Buff[1] ={0x00};
	alt_u8 Get_Buff[1] = {0x00};
    Send_Buff[0] = Flash_INS_Read_Status_Register_1;  //Status Register-1 : 05h
    alt_avalon_spi_command(FLASH_SPI_BASE, 0, 1, Send_Buff, 1, Get_Buff, 0);
    return (Get_Buff[0]&0x01);
}

void Flash_Write_Enable()
{
	alt_u8 Send_Buff[1]={0xFF};
	alt_u8 Get_Buff[1]={0xFF};
    Send_Buff[0] = Flash_INS_Write_Enable;  //Write enable 06h.
    alt_avalon_spi_command(FLASH_SPI_BASE, 0, 1, Send_Buff, 0, Get_Buff, 0);
}

void Flash_Erase(unsigned long Address,unsigned char Mode)
{
	//Erase
	//IN : Address ,Mode(0:4KB,1:32KB,2:64KB,3:whole),Byte_Mode(0:3Byte,1:4Byte)
	//OUT: ES Timecost
	alt_u8 add[4]={0};
    alt_u8 Send_Buff[5];

    memset(Send_Buff, 0xFF, sizeof(Send_Buff));
    add[0]=Address%256;
	add[1]=(Address>>8)%256;
	add[2]=(Address>>16)%256;

    switch(Mode){
		case 0:    //4KB erase
			Send_Buff[0]=Flash_INS_Sector_Erase_4KB;
			Send_Buff[1] = add[2];Send_Buff[2] = add[1];Send_Buff[3] = add[0];  //MSB first
			alt_avalon_spi_command(FLASH_SPI_BASE, 0, 4, Send_Buff, 0, NULL, 0); //Send_Buff is 4Byte.
		break;
		case 1:    //32KB erase
			Send_Buff[0]=Flash_INS_Block_Erase_32KB;
			Send_Buff[1] = add[2];Send_Buff[2] = add[1];Send_Buff[3] = add[0];  //MSB first
			alt_avalon_spi_command(FLASH_SPI_BASE, 0, 4, Send_Buff, 0, NULL, 0);        //Send_Buff is 4Byte.
		break;
		case 2:    //64KB erase
			Send_Buff[0]=Flash_INS_Block_Erase_64KB;
			Send_Buff[1] = add[2];Send_Buff[2] = add[1];Send_Buff[3] = add[0];  //MSB first
			alt_avalon_spi_command(FLASH_SPI_BASE, 0, 4, Send_Buff, 0, NULL, 0);        //Send_Buff is 4Byte.
		break;
		case 3:    //Chip erase
			Send_Buff[0]=Flash_INS_Chip_Erase;
			alt_avalon_spi_command(FLASH_SPI_BASE, 0, 1, Send_Buff, 0, NULL, 0);        //Send_Buff is 1Byte.
		break;
		default:
			printf("ES_Mode ERROR\n");
		break;
		//return 1;
	}
}

unsigned long Flash_Page_Program(unsigned long Address,spi_packet_t* data,unsigned int Length){
    //page program
	//IN : Address ,Byte_Mode(0:3Byte,1:4Byte),*fin(fread FILE*)
	//OUT: PP checksum
	alt_u8 add[3]={0};

	add[0]=Address%256;
	add[1]=(Address>>8)%256;
	add[2]=(Address>>16)%256;

	data->cmd = Flash_INS_Page_Program;  //Page Program : 02h
	data->addr[0] = add[2];data->addr[1] = add[1];data->addr[2] = add[0];  //MSB first
//	memcpy(spi_packet.data, data, Length);
	return alt_avalon_spi_command(FLASH_SPI_BASE, 0, Length+4, (alt_u8*)data, 0, NULL, 0);
}

unsigned long Flash_Read_Data(unsigned long Address,spi_packet_t* data,unsigned long Length)
{
	//Read Data
	//IN : Address,Mode(0:es checksum verify,1:pp checksum verify,2:Read Data)
	//OUT: checksum
	alt_u8 add[3]={0};

    add[0]=Address%256;
	add[1]=(Address>>8)%256;
	add[2]=(Address>>16)%256;

	data->cmd = Flash_INS_Read_Data;  //READ DATA : 03h
	data->addr[0] = add[2];data->addr[1] = add[1];data->addr[2] = add[0];  //MSB first
	return alt_avalon_spi_command(FLASH_SPI_BASE, 0, 4, (alt_u8*)data, Length, data->data, 0);
}

alt_u32 Flash_JEDEC(){

	alt_u8 Send_Buff[0x01];
	alt_u8 Get_Buff[0x03];
	alt_u32 read_data = 0;
    memset(Send_Buff, 0xFF, sizeof(Send_Buff));
    memset(Get_Buff, 0xFF, sizeof(Get_Buff));

    Send_Buff[0] = Flash_INS_JEDEC_ID;  //JEDEC ID : 9Fh
    alt_avalon_spi_command(FLASH_SPI_BASE, 0, 1, Send_Buff, 3, Get_Buff, 0);
    read_data = (Get_Buff[0]<<16)|(Get_Buff[1]<<8)|(Get_Buff[2]);

    return read_data;
}


