/*
 * spi_flash.h
 *
 *  Created on: Nov 30, 2018
 *      Author: cgage
 */

#ifndef SPI_FLASH_H_
#define SPI_FLASH_H_

#include "alt_types.h"  // alt_u32

#define MAX_FLASH_DATA_LEN 4096

#define FLASH_SPI_BASE 0x11060
//#define FLASH_SPI_BASE FLASH_SPI_BASE

#define Flash_INS_Write_Enable 0x06
#define Flash_INS_Volatile_SR_Write_Enable 0x50
#define	Flash_INS_Write_Disable 0x04
#define	Flash_INS_Read_Status_Register_1 0x05
#define Flash_INS_Read_Status_Register_2 0x35
#define Flash_INS_Read_Status_Register_3 0x15
#define Flash_INS_Write_Status_Register_1 0x01
#define Flash_INS_Write_Status_Register_2 0x31
#define Flash_INS_Write_Status_Register_3 0x11
#define Flash_INS_Sector_Erase_4KB 0x20
#define Flash_INS_Block_Erase_32KB 0x52
#define Flash_INS_Block_Erase_64KB 0xD8
#define Flash_INS_Chip_Erase 0xC7
#define Flash_INS_Read_Data 0x03
#define Flash_INS_Fast_Read 0x0B
#define Flash_INS_Page_Program 0x02
#define Flash_INS_Read_Unique_ID 0x4B
#define Flash_INS_Read_SFDP_Register 0x5A
#define Flash_INS_Erase_Security_Register 0x44
#define Flash_INS_Program_Security_Register 0x42
#define Flash_INS_Read_Security_Register 0x48
#define Flash_INS_JEDEC_ID 0x9F
#define Flash_INS_Manufacturer_ID 0x90
#define Flash_INS_Enter_4Byte_Address_Mode 0xB7
#define Flash_INS_Exit_4Byte_Address_Mode 0xE9
#define Flash_INS_Enable_Reset 0x66
#define Flash_INS_Reset_Device 0x99
#define Flash_INS_Power_Down 0xB9
#define Flash_INS_Release_Power_Down 0xAB
#define Flash_INS_Suspend 0x75
#define Flash_INS_Resume 0x7A
#define Flash_INS_Die_Select 0xC2

typedef struct {
	alt_u8 cmd;
	alt_u8 addr[3];
	alt_u8 data[MAX_FLASH_DATA_LEN];
}spi_packet_t;

alt_u8 Flash_BusyCheck();
void Flash_Write_Enable();
void Flash_Erase(unsigned long Address,unsigned char Mode);
unsigned long Flash_Page_Program(unsigned long Address,spi_packet_t* data,unsigned int Length);
unsigned long Flash_Read_Data(unsigned long Address,spi_packet_t* data,unsigned long Length);
alt_u32 Flash_JEDEC();



#endif /* SPI_FLASH_H_ */
