/*
 * main.c
 *
 *  Created on: Dec 5, 2018
 *      Author: cgage
 */

#include <stdio.h>
#include <string.h>
#include "sys/alt_stdio.h"
#include "alt_types.h"  // alt_u32
#include "system.h"
//#include "altera_avalon_spi.h"
#include "altera_avalon_spi_regs.h"

#include "spi_flash.h"
#include "cpu_uart.h"

int main()
{
	alt_u32  Address=0x000000;
	spi_packet_t spi_data;
	const int data_len = MAX_UART_LEN;
	alt_u8 * curChar = &spi_data.data[0];
	memset(curChar, 0x00, MAX_UART_LEN);
	IOWR_ALTERA_AVALON_SPI_CONTROL(FLASH_SPI_BASE,0x00);          //Initialize the spi control reg

	while(1){
		alt_u8 cmd = alt_getchar();

		if (CPU_UART_CMD_WRITE_4096 == cmd){
			for(alt_u32 byte_counter = 0; byte_counter < MAX_UART_LEN; byte_counter++){
				curChar[byte_counter] = alt_getchar();
			}
			Flash_Write_Enable();
			Flash_Page_Program(Address,&spi_data,data_len);
			while(Flash_BusyCheck());
			Address += MAX_UART_LEN;
		}else if (CPU_UART_CMD_ERASE_ALL == cmd){
			Flash_Write_Enable();
			Flash_Erase(0,3);
			while(Flash_BusyCheck());
		}else if ((CPU_UART_CMD_WRITE_START == cmd)||(CPU_UART_CMD_WRITE_END == cmd)){
			printf("Current Address: %d\n", Address);
			Address = 0;
		}else if (0x41 == cmd){
			for(alt_u32 byte_counter = 0; byte_counter < MAX_UART_LEN; byte_counter++){
				curChar[byte_counter] = alt_getchar();
			}
		}else if (CPU_UART_CMD_READ_CHECK == cmd){
			Flash_Read_Data(Address,&spi_data,data_len);
			Address += MAX_UART_LEN; //Set breakpoint here and check manually.
		}else{
			printf("Bad command: 0x%02X\n",cmd);
		}
		memset(curChar, 0x00, MAX_UART_LEN);
	}

	return 0;

	/* Test SPI Flash Operation Over */
}


