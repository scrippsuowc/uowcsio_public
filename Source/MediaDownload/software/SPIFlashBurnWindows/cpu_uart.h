/*
 * cpu_uart.h
 *
 *  Created on: Nov 30, 2018
 *      Author: cgage
 */

#ifndef CPU_UART_H_
#define CPU_UART_H_


#define CPU_UART_CMD_ERASE_ALL   0xDD
#define CPU_UART_CMD_WRITE_256  0x03
#define CPU_UART_CMD_WRITE_START 0x04
#define CPU_UART_CMD_WRITE_END   0x05
#define CPU_UART_CMD_READ_CHECK  0x10

//#define MAX_UART_LEN 128
#define MAX_UART_LEN 4096


#endif /* CPU_UART_H_ */
