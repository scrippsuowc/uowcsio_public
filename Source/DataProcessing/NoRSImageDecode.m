close all
clearvars

[Diff_V, RiseTime_uS, SR_VpuS] = FindAttenuation('.');

BitPeriod = 5000000;
MaxBitsPerByte = 9;
GuardByte = [1;-1;1;-1;1;-1;1;-1;-1];

numFiles = 6;
framesPerFile = 65;
bytesPerFrame = 4096;
numBytes = bytesPerFrame*framesPerFile*numFiles;
BytesTx = zeros(numBytes, 1);
BytesRx = zeros(numBytes, 1);

on = 1;
off = -1;
BitLen = 20;
Template00 = [off;off];
Template10 = [ on;off];
Template01 = [off; on];
Template11 = [ on; on];
Sig00 = repelem(Template00, BitLen);
Sig10 = repelem(Template10, BitLen);
Sig01 = repelem(Template01, BitLen);
Sig11 = repelem(Template11, BitLen);

for fileNum = 0:(numFiles-1)
    TxFileName = sprintf('EN0_00%d_Ch1.wfm', fileNum);
    RxFileName = sprintf('EN0_00%d_Ch2.wfm', fileNum);

    TxSignal = wfm2read(TxFileName);
    [RxSignal, ~, Info] = wfm2read(RxFileName);

    SamplePeriod = 1/Info.samplingrate;
    SamplesPerBit = 1/(BitPeriod*SamplePeriod);
    GuardMatch = repelem(GuardByte, SamplesPerBit);
    GuardBitsLen = length(GuardMatch);

    RxSignal = lowpass(RxSignal, BitPeriod, Info.samplingrate);
    
    mfCheckTxStartInd = 300000;
    mfCheckTxEndInd = 310000;
    mfCheckRxStartInd = 300000;
    mfCheckRxEndInd = 310000;

    for frame = 0:(framesPerFile-1)
        mfCheckTxLen = mfCheckTxEndInd-mfCheckTxStartInd+1;
        mfCheckRxLen = mfCheckRxEndInd-mfCheckRxStartInd+1;

        [mfTx, ~] = matchedFilter(TxSignal(mfCheckTxStartInd:mfCheckTxEndInd), GuardMatch, mfCheckTxLen,  GuardBitsLen);
        mfTx(mfTx<0.8*max(mfTx)) = 0;
        [~, locTx] = findpeaks(mfTx);
        startIndTx = locTx(1)+mfCheckTxStartInd-1;
        [mfRx, ~] = matchedFilter(RxSignal(mfCheckRxStartInd:mfCheckRxEndInd), GuardMatch, mfCheckRxLen,  GuardBitsLen);
        mfRx(mfRx<0.8*max(mfRx)) = 0;
        [~, locRx] = findpeaks(mfRx);
        startIndRx = locRx(1)+mfCheckRxStartInd;

        endOneTx = startIndTx+(bytesPerFrame+2)*MaxBitsPerByte*SamplesPerBit;
        endOneRx = startIndRx+(bytesPerFrame+2)*MaxBitsPerByte*SamplesPerBit;
        OneTxSignal = TxSignal(startIndTx:endOneTx);
        OneRxSignal = RxSignal(startIndRx:endOneRx);
        shift = (max(OneTxSignal)+min(OneTxSignal))/2;
        OneTxSignal = OneTxSignal - shift; %Make the signal double-ended
        shift = (max(OneRxSignal)+min(OneRxSignal))/2;
        OneRxSignal = OneRxSignal - shift; %Make the signal double-ended    

        frameOffset = bytesPerFrame*frame;
        fileOffset = bytesPerFrame*framesPerFile*fileNum;
        for i = 1:bytesPerFrame
           curInd = i+frameOffset+fileOffset;
           StartInd = 1 + MaxBitsPerByte*SamplesPerBit*i;
           StartInd = StartInd - floor(StartInd*0.00001);
           % This floor section is needed because the signal shifts left by
           % about 1 bit every 100,000 bits (in other words, the pll generating
           % the 10MHz clock is slow by ~0.001%)
           EndInd = StartInd + (MaxBitsPerByte-1)*SamplesPerBit-1;
           BytesTx(curInd) =  ReadByteMFOOK(OneTxSignal(StartInd:EndInd),Sig00,Sig01,Sig10,Sig11);
           BytesRx(curInd) =  ReadByteMFOOK(OneRxSignal(StartInd:EndInd),Sig00,Sig01,Sig10,Sig11);
        end

        mfCheckTxStartInd = startIndTx+EndInd+0.5*MaxBitsPerByte*SamplesPerBit;
        mfCheckTxEndInd = mfCheckTxStartInd+20*SamplesPerBit;
        mfCheckRxStartInd = startIndRx+EndInd+0.5*MaxBitsPerByte*SamplesPerBit;
        mfCheckRxEndInd = mfCheckRxStartInd+20*SamplesPerBit;
    end
end

BER = GetBER(BytesTx, BytesRx,numBytes);

dye0RxID = fopen('Fishes0Rx.jpg', 'w');
dye0TxID = fopen('Fishes0Tx.jpg', 'w');
fwrite(dye0RxID, BytesRx(1:289027));
fwrite(dye0TxID, BytesTx(1:289027));
fclose(dye0RxID);
fclose(dye0TxID);

dye0RxID = fopen('Fishes1Rx.jpg', 'w');
dye0TxID = fopen('Fishes1Tx.jpg', 'w');
fwrite(dye0RxID, BytesRx(289028:578984));
fwrite(dye0TxID, BytesTx(289028:578984));
fclose(dye0RxID);
fclose(dye0TxID);

dye0RxID = fopen('Fishes2Rx.jpg', 'w');
dye0TxID = fopen('Fishes2Tx.jpg', 'w');
fwrite(dye0RxID, BytesRx(578985:874358));
fwrite(dye0TxID, BytesTx(578985:874358));
fclose(dye0RxID);
fclose(dye0TxID);

dye0RxID = fopen('Fishes3Rx.jpg', 'w');
dye0TxID = fopen('Fishes3Tx.jpg', 'w');
fwrite(dye0RxID, BytesRx(874359:1168530));
fwrite(dye0TxID, BytesTx(874359:1168530));
fclose(dye0RxID);
fclose(dye0TxID);

dye0RxID = fopen('Fishes4Rx.jpg', 'w');
dye0TxID = fopen('Fishes4Tx.jpg', 'w');
fwrite(dye0RxID, BytesRx(1168531:1461378));
fwrite(dye0TxID, BytesTx(1168531:1461378));
fclose(dye0RxID);
fclose(dye0TxID);

plot(mfTx);
hold on;
plot(mfRx);
figure
plot(OneTxSignal);
hold on;
plot(OneRxSignal);
figure
plot(BytesTx);
hold on;
plot(BytesRx);
figure
plot(BytesTx-BytesRx);
