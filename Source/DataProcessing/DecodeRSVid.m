clearvars
close all

k = 223;
n = 255;

readName = sprintf('VideoTx.rsenc');
writeName = sprintf('DecodeVid/VideoTx.mov');
readFile = fopen(readName);

fseek(readFile, 0,'eof');
filelen = ftell(readFile);
numRows = ceil(filelen/n);
fseek(readFile, 0,'bof');
pic = fread(readFile, [n, numRows],'uint8');
pic = pic.';
fclose(readFile);

msg = gf(pic, 8);

decoded = rsdec(msg,n,k);

writeFile = fopen(writeName,'w');
fwrite(writeFile, decoded.x.','uint8');
fclose(writeFile);

readName = sprintf('VideoRx.rsenc');
writeName = sprintf('DecodeVid/VideoRx.mov');
readFile = fopen(readName);

fseek(readFile, 0,'eof');
filelen = ftell(readFile);
numRows = ceil(filelen/n);
fseek(readFile, 0,'bof');
pic = fread(readFile, [n, numRows],'uint8');
pic = pic.';
fclose(readFile);

msg = gf(pic, 8);

decoded = rsdec(msg,n,k);

writeFile = fopen(writeName,'w');
fwrite(writeFile, decoded.x.','uint8');
fclose(writeFile);
