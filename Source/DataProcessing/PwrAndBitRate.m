function [BpJ,Watts,RealMbps] = PwrAndBitRate(Encoding,numClkDivs)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

SigFN = sprintf('../Power/EN%d_Ch1.wfm', Encoding);
DiffVFN = sprintf('../Power/EN%d_Ch3.wfm', Encoding);
TotVFN = sprintf('../Power/EN%d_Ch4.wfm', Encoding);

SignalTx = wfm2read(SigFN);
[DiffV, T_V, Info] = wfm2read(DiffVFN);
InvTotalV = wfm2read(TotVFN);

TotalV = -1.*(InvTotalV - 4.8);
Current = DiffV./0.02;
Power = TotalV.*Current;
[FirstDivide, SecondDivide, LastDivide] = FindPartitions(SignalTx,0.5);

BpJ = zeros(numClkDivs,1);
Watts = zeros(numClkDivs,1);
RealMbps = zeros(numClkDivs,1);
Starts = [90000 FirstDivide SecondDivide];
Ends = [FirstDivide-250 SecondDivide-250 LastDivide-250];

for i = 1:numClkDivs
    T_total = single(Ends(i)-Starts(i))/Info.samplingrate;
    RealMbps(i) = 4096*8/T_total;

    Temp_Power = Power(Starts(i):Ends(i));
    Temp_T_V = T_V(Starts(i):Ends(i));
    Energy = trapz(Temp_T_V, Temp_Power);
    EpB = Energy/(4096*8);
    BpJ(i) = 1/EpB;
    Watts(i) = RealMbps(i)/BpJ(i);
    RealMbps(i) = RealMbps(i)/(10^6);
end

end

