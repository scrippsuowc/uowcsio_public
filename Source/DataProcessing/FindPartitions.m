function [Partition1,Partition2,PartitionEnd] = FindPartitions(Signal,MaxFrac)
%FindPartitions Finds the locations where the signal clock division changes
%   When we are running multiple runs at once, we need to separate out the
%   different clock regimes, this function finds the location of those
%   changes. It basically looks for the first two locations where there are
%   more then 25 bits worth (at 10MHz) of 0's being transmitted, as during 
%   a normal 10 and 5 MHz transmission, there should be no period that long

bitTime = 250;
Threshold = MaxFrac*max(Signal)+(1-MaxFrac)*min(Signal); %max - (1-MaxFrac)(max-min)
sigBZ = Signal<Threshold;
sigPrevSum = zeros(length(sigBZ),1);
lastSum = 0;

for i = 1:length(sigBZ)
    lastSum = (lastSum + sigBZ(i)) * sigBZ(i);
    sigPrevSum(i) = lastSum;
end

sigPrevSumLocs = find(sigPrevSum>bitTime);
sigPrevLocsDiff = diff(sigPrevSumLocs);
sigPrevLocsDiff = sigPrevLocsDiff>5;
sigPrevLocsDiff = logical([0; sigPrevLocsDiff]);
sigPrevSumLocs = sigPrevSumLocs(sigPrevLocsDiff);

Partition1 = int32(sigPrevSumLocs(1));%-bitTime);
Partition2 = int32(sigPrevSumLocs(2));%-bitTime);
PartitionEnd = int32(sigPrevSumLocs(end));

end

