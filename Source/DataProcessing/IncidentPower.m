close all
clearvars

DyeCounts = [0 1 2 3 4 5 6 8 10 12];
Diff_V = zeros(1,length(DyeCounts));
Sig = wfm2read('Dye0/Pulse1Hz_Ch1.wfm');

for i = 1:length(DyeCounts)
    fileStr = sprintf('Dye%d/Pulse1Hz_Ch2.wfm', DyeCounts(i));
    [Pwr, ~, Info] = wfm2read(fileStr);
    Pwr = lowpass(Pwr, 10000000, Info.samplingrate);

    FirstThirdInd = floor(Info.nop/3);
    LastThirdInd = floor(2*Info.nop/3);

    low = mean(Pwr(1:FirstThirdInd));
    high = mean(Pwr(LastThirdInd:end));
    Diff_V(i) = high - low;
    if (i == 1)
        ShortPwr = Pwr(450:600);
        ten = Diff_V(1).*0.1+low;
        sixty = Diff_V(1).*0.6+low;
        ninety = Diff_V(1).*0.9+low;
        x = linspace(-0.5,1,151);
        figure
        yyaxis right;
        plot(x,Sig(450:600));
        ylabel("FPGA Signal (V)");
        hold on;
        yyaxis left;
        plot(x,ShortPwr);
        ylabel("Incident Light (mV)");
        title("Rise Time of Transmitter");
        xlabel("Trigger Offset (\mus)");
        xlim([-0.5 1]);
        xline(0.115,'-',{'10% Rise', 'Level at', '115 ns'}, 'LabelHorizontalAlignment', 'left', 'LabelVerticalAlignment', 'middle', 'LabelOrientation', 'horizontal');
        xline(0.145,'-',{'60% Rise', 'Level at', '145 ns'}, 'LabelHorizontalAlignment', 'right', 'LabelVerticalAlignment', 'middle', 'LabelOrientation', 'horizontal');
        xline(0.54,'-',{'90% Rise', 'Level at', '540 ns'}, 'LabelHorizontalAlignment', 'right', 'LabelVerticalAlignment', 'middle', 'LabelOrientation', 'horizontal');
        grid on;
    end
end

IncPwr = 30+10.*log10(Diff_V.*0.0022);
RelInt = Diff_V./Diff_V(1);
p = polyfit(RelInt, IncPwr, 4);