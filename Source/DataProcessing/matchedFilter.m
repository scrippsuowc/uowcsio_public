function [x, tau] = matchedFilter(S, E, N, L)

maxTau = N-L;
x = zeros(1,maxTau);
tau = 1:maxTau;
for i = 1:maxTau
   x(i) = sum(S(i+1:i+L).*E);
end
