function [BER, perByteErrors] = GetBER(BytesTx,BytesRx,numBytes)
%GetBER Finds the BER and number of bit errors for each 
%   Detailed explanation goes here

xored = bitxor(BytesTx, BytesRx);
bitErrs = 0;
perByteErrors = zeros(256, 1);
for i = 1:numBytes
    curErrs = sum(bitget(xored(i),1:8));
    bitErrs = bitErrs + curErrs;
    j = BytesTx(i)+1;
    perByteErrors(j) = perByteErrors(j) + curErrs;
end
BER = bitErrs/(8*numBytes);

end

