function [data, bitsUsed] = ReadByteMFDPIM(ByteSignal)
%READBYTEMFDPIM Summary of this function goes here
%   Detailed explanation goes here

on = 1;
off = -1;

Template101     = [on;off;on];
Template1001    = [on;off;off;on];
Template10001   = [on;off;off;off;on];
Template100001  = [on;off;off;off;off;on];
Template1000001 = [on;off;off;off;off;off;on];

SigLen = length(ByteSignal);
BitLen = SigLen/22;

Sig101     = repelem(Template101,     BitLen);
Sig1001    = repelem(Template1001,    BitLen);
Sig10001   = repelem(Template10001,   BitLen);
Sig100001  = repelem(Template100001,  BitLen);
Sig1000001 = repelem(Template1000001, BitLen);

numBits = [ 3; 4; 5; 6 ];
demiNibbleLen = BitLen.*numBits;
startIndex = 1;

data = 0;

for i = 1:4
    endIndex = startIndex + demiNibbleLen - 1;
    if i == 4
        endIndex = endIndex+BitLen;
        MF =  [sum(ByteSignal(startIndex:endIndex(1)).*Sig1001);
               sum(ByteSignal(startIndex:endIndex(2)).*Sig10001);
               sum(ByteSignal(startIndex:endIndex(3)).*Sig100001);
               sum(ByteSignal(startIndex:endIndex(4)).*Sig1000001)];
        MF = MF./(numBits+1);
    else
        MF =  [sum(ByteSignal(startIndex:endIndex(1)).*Sig101);
               sum(ByteSignal(startIndex:endIndex(2)).*Sig1001);
               sum(ByteSignal(startIndex:endIndex(3)).*Sig10001);
               sum(ByteSignal(startIndex:endIndex(4)).*Sig100001)];
        MF = MF./numBits;
    end
    [~,MaxInd] = max(MF);
    data = data + ((MaxInd-1)*2^(2*i-2));
    startIndex = startIndex + demiNibbleLen(MaxInd)-BitLen;
end

bitsUsed = (startIndex+BitLen-1)/BitLen;

end

