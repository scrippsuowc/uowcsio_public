clearvars

k = 223;
n = 255;

for i = 0:4
    readName = sprintf('Fishes%dTx.rsenc', i);
    writeName = sprintf('DecodedPics/Fishes%dTx.jpg', i);
    readFile = fopen(readName);

    fseek(readFile, 0,'eof');
    filelen = ftell(readFile);
    numRows = ceil(filelen/n);
    fseek(readFile, 0,'bof');
    pic = fread(readFile, [n, numRows],'uint8');
    pic = pic.';
    fclose(readFile);

    msg = gf(pic, 8);

    decoded = rsdec(msg,n,k);

    writeFile = fopen(writeName,'w');
    fwrite(writeFile, decoded.x.','uint8');
    fclose(writeFile);
end
