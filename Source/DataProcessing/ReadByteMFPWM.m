function data = ReadByteMFPWM(ByteSignal)
%READBYTEMFPWM Summary of this function goes here
%   Detailed explanation goes here

on = 1;
off = -1;

Template1000 = [ on;off;off;off];
Template1100 = [ on; on;off;off];
Template1110 = [ on; on; on;off];
Template1111 = [ on; on; on; on];

SigLen = length(ByteSignal);
BitLen = SigLen/8;
MFLen  = 2*BitLen;

Sig1000 = repelem(Template1000, BitLen/2);
Sig1100 = repelem(Template1100, BitLen/2);
Sig1110 = repelem(Template1110, BitLen/2);
Sig1111 = repelem(Template1111, BitLen/2);

data = 0;

for i = 1:4
    startIndex = (i-1)*MFLen+1;
    endIndex = startIndex + MFLen - 1;
    MF =  [sum(ByteSignal(startIndex:endIndex).*Sig1000);
           sum(ByteSignal(startIndex:endIndex).*Sig1100);
           sum(ByteSignal(startIndex:endIndex).*Sig1110);
           sum(ByteSignal(startIndex:endIndex).*Sig1111)];
    [~,MaxInd] = max(MF);
    data = data + ((MaxInd-1)*2^(2*i-2));
end

end

