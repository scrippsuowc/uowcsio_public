close all;
clearvars;

baseFolderStr = "../181221/";
curFolderStr = sprintf("%s%s", baseFolderStr, "PureWater");
numEncodings = 4;
numClkDivs = 2;
DataClkMbps = [10 5];

PureAtten = FindAttenuation(curFolderStr);
PureBER = zeros(numEncodings,numClkDivs);
BpJ = zeros(numEncodings,numClkDivs);
Watts = zeros(numEncodings,numClkDivs);
RealMbps = zeros(numEncodings,numClkDivs);
for j = 1:numEncodings
    enc = j-1;
    PureBER(j,:) = DecodeOneEncoding(enc, curFolderStr, 1, numClkDivs);
    [BpJ(j,:), Watts(j,:), RealMbps(j,:)] = PwrAndBitRate(enc,numClkDivs);
end
figure(1)
plot(RealMbps(1,:), BpJ(1,:),'k-*','MarkerSize',10);
hold on;
plot(RealMbps(2,:), BpJ(2,:),'b-o','MarkerSize',10);
plot(RealMbps(3,:), BpJ(3,:),'r-+','MarkerSize',10);
plot(RealMbps(4,:), BpJ(4,:),'m-s','MarkerSize',10);
xlabel("Data Throughput (Mbps)");
xlim([0 10]);
ylabel("Power Efficiency (Bits per Joule)");
title(['Power Efficiency' 10 'vs. Data Throughput']);
lgd = legend("OOK", "PWM", "PPM", "DPIM", 'Location', 'southeast');
title(lgd,"Encoding");
grid on;

figure(2)
plot(RealMbps(1,:), Watts(1,:),'k*','MarkerSize',10);
hold on;
plot(RealMbps(2,:), Watts(2,:),'bo','MarkerSize',10);
plot(RealMbps(3,:), Watts(3,:),'r+','MarkerSize',10);
plot(RealMbps(4,:), Watts(4,:),'ms','MarkerSize',10);
xlabel("Data Throughput (Mbps)");
xlim([0 10]);
ylabel("Continuous Power (Watts)");
title(['Continuous Power' 10 'vs. Data Throughput']);
lgd = legend("OOK", "PWM", "PPM", "DPIM", 'Location', 'southeast');
title(lgd,"Encoding");
grid on;

figure(3)
plot(DataClkMbps,RealMbps(1,:),'k*','MarkerSize',10);
hold on;
plot(DataClkMbps,RealMbps(2,:),'bo','MarkerSize',10);
plot(DataClkMbps,RealMbps(3,:),'r+','MarkerSize',10);
plot(DataClkMbps,RealMbps(4,:),'ms','MarkerSize',10);
xlim([0 10.1]);
xlabel("Data Clock Rate (MHz)");
ylim([0 10]);
ylabel("Data Throughput (Mbps)");
title(['Data Throughput' 10 'vs. Data Clock Rate']);
lgd = legend("OOK", "PWM", "PPM", "DPIM", 'Location', 'southeast');
title(lgd,"Encoding");
grid on;

figure(4)
plot(4,mean(RealMbps(1,:)./DataClkMbps),'k*','MarkerSize',10);
hold on;
plot(1,mean(RealMbps(2,:)./DataClkMbps),'bo','MarkerSize',10);
plot(2,mean(RealMbps(3,:)./DataClkMbps),'r+','MarkerSize',10);
plot(3,mean(RealMbps(4,:)./DataClkMbps),'ms','MarkerSize',10);
xlim([0 5]);
xticks([1 2 3 4]);
xticklabels({'PWM','PPM','DPIM','OOK'});
xlabel("Encoding");
ylim([0 1]);
ylabel("Data Throughput Reduction");
title(['Reduction in Data Throughput' 10 'Due to Encoding Scheme']);
grid on;


numDyeDrops = 10;
DyeAttens = zeros(numDyeDrops, 1);
DyeBERs = zeros(numDyeDrops, numEncodings, numClkDivs);
for i = 1:numDyeDrops
   curFolderStr = sprintf("%s%s%d",baseFolderStr,"Dye",i); 
   DyeAttens(i) = FindAttenuation(curFolderStr);
   if DyeAttens(i)/PureAtten > 0.5
       useAL = 1;
   else
       useAL = 0;
   end
   for j = 1:numEncodings
      enc = j-1;
      DyeBERs(i,j,:) = DecodeOneEncoding(enc, curFolderStr, useAL, numClkDivs);
   end
end
RelInt = DyeAttens./PureAtten;
RelInttoIncPwrPoly = [-9.6383 31.9757 -42.8106 32.3367 -41.0792];
IncPwr = polyval(RelInttoIncPwrPoly, [1; RelInt]);

encStrs = ["OOK" "PWM" "PPM" "DPIM"];
for j = 1:numEncodings
   figure(10+j);
   plot(IncPwr, [PureBER(j,1); DyeBERs(:,j,1)],'k-*','MarkerSize',10);
   hold on;
   plot(IncPwr, [PureBER(j,2); DyeBERs(:,j,2)],'b--o','MarkerSize',10);
   set(gca, 'YScale', 'log');
   xlabel("Incident Power (dBm)");
   xlim([-40 -28]);
   ylabel("Bit Error Rate");
   ylim([10^-5 1]);
   title(['Bit Error Rate vs. Relative Intensity' 10 'for ' char(encStrs(j)) ' Encoding']);
   lgd = legend([num2str(RealMbps(j,1),3) ' Mbps'], [num2str(RealMbps(j,2),3) ' Mbps']);
   title(lgd, "Data Throughput");
   grid on;
end

figure(20);
X = IncPwr;
stem3(X,RealMbps(1,:),[PureBER(1,:);reshape(DyeBERs(:,1,:),10,2)].','k*','MarkerSize',10);
hold on
stem3(X,RealMbps(2,:),[PureBER(2,:);reshape(DyeBERs(:,2,:),10,2)].','bo','MarkerSize',10);
stem3(X,RealMbps(3,:),[PureBER(3,:);reshape(DyeBERs(:,3,:),10,2)].','r+','MarkerSize',10);
stem3(X,RealMbps(4,:),[PureBER(4,:);reshape(DyeBERs(:,4,:),10,2)].','ms','MarkerSize',10);
xlabel("Incident Power (dBm)");
xlim([-40 -28]);
ylabel("Data Throughput (Mbps)");
zlabel("Bit Error Rate");
lgd = legend("OOK", "PWM", "PPM", "DPIM", 'Location', 'east');
title(lgd,"Encoding");
set(gca, 'ZScale', 'log');

figure(21);
plot(RealMbps(1,:),[max(DyeBERs(:,1,1));max(DyeBERs(:,1,2))],'k*','MarkerSize',10);
hold on;
plot(RealMbps(2,:),[max(DyeBERs(:,2,1));max(DyeBERs(:,2,2))],'bo','MarkerSize',10);
plot(RealMbps(3,:),[max(DyeBERs(:,3,1));max(DyeBERs(:,3,2))],'r+','MarkerSize',10);
plot(RealMbps(4,:),[max(DyeBERs(:,4,1));max(DyeBERs(:,4,2))],'ms','MarkerSize',10);
xlabel("Data Throughput (Mbps)");
ylabel("Maximum Bit Error Rate");
ylim([10^-5 1]);
title(['Maximum Bit Error Rate vs. Data Throughput' 10 'for All Encodings']);
lgd = legend("OOK", "PWM", "PPM", "DPIM", 'Location', 'northeast');
title(lgd,"Encoding");
set(gca, 'YScale', 'log');
grid on;

figure(22);
plot(RealMbps(1,:),[mean(DyeBERs(:,1,1));mean(DyeBERs(:,1,2))],'k*','MarkerSize',10);
hold on;
plot(RealMbps(2,:),[mean(DyeBERs(:,2,1));mean(DyeBERs(:,2,2))],'bo','MarkerSize',10);
plot(RealMbps(3,:),[mean(DyeBERs(:,3,1));mean(DyeBERs(:,3,2))],'r+','MarkerSize',10);
plot(RealMbps(4,:),[mean(DyeBERs(:,4,1));mean(DyeBERs(:,4,2))],'ms','MarkerSize',10);
xlabel("Data Throughput (Mbps)");
ylabel("Mean Bit Error Rate");
ylim([10^-5 1]);
title(['Mean Bit Error Rate vs. Data Throughput' 10 'for All Encodings']);
lgd = legend("OOK", "PWM", "PPM", "DPIM", 'Location', 'northeast');
title(lgd,"Encoding");
set(gca, 'YScale', 'log');
grid on;