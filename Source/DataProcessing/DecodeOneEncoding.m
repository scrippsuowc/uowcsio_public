function BER = DecodeOneEncoding(Encoding, folderStr, useAutoLevel, clkDivs)
%DecodeOneEncoding Finds BER, BpJ, and RealMbps for one encoding file

BER = zeros(clkDivs,1);

if Encoding == 0
    GuardByte = [1;-1;1;-1;1;-1;1;-1;-1];
    MaxBitsPerByte = 9;
    MFRxThresh = 0.6;
elseif Encoding == 1
    GuardByte = [1;-1;-1;-1;1;1;-1;-1;1;1;1;-1;1;1;1;1];
    MaxBitsPerByte = 17;
    MFRxThresh = 0.6;
elseif Encoding == 2
   GuardByte = [1;-1;-1;-1;-1;1;-1;-1;-1;-1;1;-1;-1;-1;-1;1];
   MaxBitsPerByte = 17;
   MFRxThresh = 0.95;
elseif Encoding == 3
    GuardByte = [1;-1;1;-1;1;-1;1;-1;-1];
    MaxBitsPerByte = 21;
    MFRxThresh = 0.5;
end

TxFileName = sprintf('%s/EN%d_Ch1.wfm', folderStr, Encoding);
RxFileName = sprintf('%s/EN%d_Ch2.wfm', folderStr, Encoding);
OriginalSignalTx = wfm2read(TxFileName);
[OriginalSignalRx, ~, Info] = wfm2read(RxFileName);

[FirstDivide, SecondDivide, LastDivide] = FindPartitions(OriginalSignalTx,0.5);
StartInds = [1 FirstDivide SecondDivide];
EndInds = [FirstDivide SecondDivide LastDivide];

for j = 1:clkDivs
    BitPeriod = 10000000/(2^(j-1));
    
    try
        [SignalRx, SamplesPerBit] = IsolateSignal(OriginalSignalRx(StartInds(j):EndInds(j)),BitPeriod,GuardByte,MaxBitsPerByte,Info,MFRxThresh,Encoding);
    catch
        warning(['Error Isolating Rx Signal for EN', num2str(Encoding),...
            ', CD', num2str(j-1), ' in ', char(folderStr)]);
        SamplesPerBit = 5*2^j;
        SignalRx = zeros(4098*MaxBitsPerByte*SamplesPerBit, 1);
    end
    
    if Encoding == 0
        SigRxMaxDiff = max(SignalRx)-min(SignalRx);
    end        
    
    BytesTx = zeros(4096, 1);
    BytesRx = zeros(4096, 1);
    DPIMBitsUsed = length(GuardByte);
    for i = 1:4096
       if Encoding == 3
          StartInd = 1 + DPIMBitsUsed*SamplesPerBit;
       else
          StartInd = 1 + MaxBitsPerByte*SamplesPerBit*i;
       end
       StartInd = StartInd - floor(StartInd*0.00001);
       % This floor section is needed because the signal shifts left by
       % about 1 bit every 100,000 bits (in other words, the pll generating
       % the 10MHz clock is slow by ~0.001%)
       EndInd = StartInd + (MaxBitsPerByte-1)*SamplesPerBit-1;
       BytesTx(i) =  bitand(i-1,255);
       if Encoding == 0           
           %autoLevel
           if 1 == useAutoLevel
               diff = max(SignalRx(StartInd:EndInd+SamplesPerBit)) - min(SignalRx(StartInd:EndInd+SamplesPerBit));
               if diff >= 0.4*SigRxMaxDiff
                   SignalRx(StartInd:EndInd+SamplesPerBit) = SignalRx(StartInd:EndInd+SamplesPerBit) - (max(SignalRx(StartInd:EndInd+SamplesPerBit)) + min(SignalRx(StartInd:EndInd+SamplesPerBit)))/2;
               end
           end
           BytesRx(i) =  ReadByteMFOOK(SignalRx(StartInd:EndInd));
       elseif Encoding == 1
           BytesRx(i) =  ReadByteMFPWM(SignalRx(StartInd:EndInd));
       elseif Encoding == 2
           BytesRx(i) =  ReadByteMFPPM(SignalRx(StartInd:EndInd));
       elseif Encoding == 3
           EndInd = StartInd + (MaxBitsPerByte+1)*SamplesPerBit-1;
           [BytesRx(i), bitsUsed] =  ReadByteMFDPIM(SignalRx(StartInd:EndInd));
           DPIMBitsUsed = DPIMBitsUsed + bitsUsed;
       end
    end
    BER(j) = GetBER(BytesTx, BytesRx, 4096);
end

end

