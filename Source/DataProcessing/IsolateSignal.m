function [SigOut, SamplesPerBit] = IsolateSignal(Signal,BitPeriod,GuardByte,MaxBitsPerByte,Info,MFThresh,Enc)
%IsolateSignal Returns the signal, starting at the first sample of the
%first guard byte and ending at the last sample of the last guard byte

shift = mean(Signal(Signal>(0.1+mode(Signal))))/2;%
Signal = Signal - shift; %Make the signal double-ended

SamplePeriod = 1/Info.samplingrate;
SamplesPerBit = 1/(BitPeriod*SamplePeriod);
GuardMatch = repelem(GuardByte, SamplesPerBit);
GuardBitsLen = length(GuardMatch);

Signal = lowpass(Signal, BitPeriod, Info.samplingrate);

[mf, ~] = matchedFilter(Signal, GuardMatch, length(Signal),  GuardBitsLen);
mf = mf-mean(mf);
mfMaxs = mf.*(mf>MFThresh*max(mf));

[~, loc] = findpeaks(mfMaxs);

firstInd = loc(1);
if Enc == 3
    lastInd = loc(end) + GuardBitsLen;
else
    lastInd = firstInd + 4098*MaxBitsPerByte*SamplesPerBit;
end

% If we have detected the wrong firstInd (found a guard byte further along
% in the file) decrease the MFThresh until we find one that works.
while lastInd > length(Signal)
    MFThresh = MFThresh - 0.01;
    mfMaxs = mf.*(mf>MFThresh*max(mf));

    [~, loc] = findpeaks(mfMaxs);

    firstInd = loc(1);
    lastInd = firstInd + 4098*MaxBitsPerByte*SamplesPerBit; 
end

Signal = Signal(firstInd:lastInd);
shift = mean(Signal);%
SigOut = Signal - shift; %Make the signal double-ended
end

