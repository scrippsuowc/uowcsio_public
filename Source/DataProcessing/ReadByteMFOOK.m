function data = ReadByteMFOOK(ByteSignal)
%READBYTEMFOOK Summary of this function goes here
%   Detailed explanation goes here

on = 0.25;
off = -1;

Template00 = [off;off];
Template10 = [ on;off];
Template01 = [off; on];
Template11 = [ on; on];

SigLen = length(ByteSignal);
BitLen = SigLen/8;
MFLen  = 2*BitLen;

Sig00 = repelem(Template00, BitLen);
Sig10 = repelem(Template10, BitLen);
Sig01 = repelem(Template01, BitLen);
Sig11 = repelem(Template11, BitLen);

data = 0;

for i = 1:4
    startIndex = (i-1)*MFLen+1;
    endIndex = startIndex + MFLen - 1;
    MF =  [sum(ByteSignal(startIndex:endIndex).*Sig00);
           sum(ByteSignal(startIndex:endIndex).*Sig10);
           sum(ByteSignal(startIndex:endIndex).*Sig01);
           sum(ByteSignal(startIndex:endIndex).*Sig11)];
    [~,MaxInd] = max(MF);
    data = data + ((MaxInd-1)*2^(2*i-2));
end

end

