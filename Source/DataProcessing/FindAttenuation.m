function [Diff_V, RiseTime_uS, SR_VpuS] = FindAttenuation(folderStr)
%FindAttenuation Finds the absolute magnitude difference between on and off
%                states. Also finds the rise time and 10%-90% slew rate.

fileStr = sprintf('%s/Attenuation_Ch2.wfm', folderStr);
[Att, ~, Info] = wfm2read(fileStr);
Att = lowpass(Att, 10000000, Info.samplingrate);

FirstThirdInd = floor(Info.nop/3);
LastThirdInd = floor(2*Info.nop/3);

low = mean(Att(1:FirstThirdInd));
high = mean(Att(LastThirdInd:end));
Diff_V = high - low;

AttShifted = Att(FirstThirdInd:LastThirdInd)-low;
LowThreshold = Diff_V/10;
HighThreshold = LowThreshold*9;

FirstLow = find(AttShifted>LowThreshold);
FirstLow = FirstLow(1);
FirstHigh = find(AttShifted>HighThreshold);
FirstHigh = FirstHigh(1);
RiseTime_uS = 10^6*(FirstHigh-FirstLow)/Info.samplingrate;
SR_VpuS = 0.8*Diff_V / RiseTime_uS;

end

