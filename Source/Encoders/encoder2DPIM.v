`timescale 1 ns/ 1 ps
module encoder2DPIM (
input wire interface_clk,
input wire modem_clk,
input wire tx_en,
input wire [7:0] data_in,
input wire data_valid,

output reg sig_out,
output reg tx_ready
);

reg [18:0] data;
reg [7:0]  out_counter;
reg old_data_valid, data_valid_latch, data_valid_ack;

always @(posedge interface_clk or negedge tx_en) begin
    if (tx_en == 0) begin
        old_data_valid <= 0;
        data_valid_latch <= 0;
    end
    else begin
        old_data_valid <= data_valid;
        if (data_valid && (data_valid != old_data_valid)) begin
            data_valid_latch <= 1;
        end
        if (data_valid_ack) begin
            data_valid_latch <= 0;
        end
    end
end

always @ (posedge modem_clk or negedge tx_en) begin
    if (tx_en == 0) begin
        sig_out <= 1'b0;
        data <= 18'd0;
        tx_ready <= 0;
        out_counter <= 0;
        data_valid_ack <= 0;
    end
    else begin
        if (data_valid_latch) begin
            sig_out <= 1;
            data[data_in[1:0] + 5'd1] <= 1;
            data[data_in[3:2] + data_in[1:0] + 5'd3] <= 1;
            data[data_in[5:4] + data_in[3:2] + data_in[1:0] + 5'd5] <= 1;
            out_counter <= data_in[7:6] + data_in[5:4] + data_in[3:2] + data_in[1:0] + 8'd7;
            tx_ready <= 0;
            data_valid_ack <= 1;
        end
        else if (out_counter == 0) begin
            sig_out <= 0;
            tx_ready <= 1;
            data_valid_ack <= 0;
        end
        else begin
            sig_out <= data[0];
            data <= {1'b0, data[18:1]};
            out_counter <= out_counter - 8'd1;
            tx_ready <= 0;
            data_valid_ack <= 0;
        end
    end
end

endmodule