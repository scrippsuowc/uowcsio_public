module SPIMaster(
input wire reset,
input wire clk,
input wire [7:0] data_in,
input wire data_valid,
input wire cont_tx,
output reg [7:0] data_out,
output reg tx_ready,

input wire MISO,
output reg SCLK,
output reg SS_n,
output reg MOSI
);

reg [7:0] data_latch;
reg [3:0] bit_counter;

always @(posedge clk or posedge reset) begin
    if (reset) begin
        SCLK        <= 1'b1;
    end
    else if (1'b0 == tx_ready) begin
        if (SCLK) begin
            SCLK <= 1'b0;
        end
        else begin
            SCLK <= 1'b1;
        end
    end
    else begin
        SCLK <= 1'b1;
    end
end

always @(negedge clk or posedge reset) begin
    if (reset) begin
        tx_ready    <= 1'b1;
        SS_n        <= 1'b1;
        MOSI        <= 1'b1;
        bit_counter <= 4'd0;
        data_out    <= 8'hff;
        data_latch  <= 8'hff;        
    end
    else if ((1'b1==tx_ready)&&(1'b1==data_valid)) begin
        SS_n = 1'b0;
        tx_ready <= 1'b0;
        data_latch <= data_in;
    end
    else if (1'b0 == tx_ready) begin
        if (SCLK) begin
            data_out <= {data_out[6:0], MISO};
            if (4'd8 == bit_counter) begin
                tx_ready <= 1'b1;
                bit_counter <= 4'd0;
                if (~cont_tx) begin
                    SS_n = 1'b1;
                end
            end
        end
        else begin
            MOSI <= data_latch[7];
            data_latch <= {data_latch[6:0], 1'b0};
            bit_counter <= bit_counter + 4'b1;
        end
    end
end


endmodule