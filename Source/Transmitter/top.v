module top (
    input  wire clk_12M,
    input  wire reset_n,
    input  wire vid_data_MISO,
    output wire vid_data_MOSI,
    output wire vid_data_SCLK,
    output wire vid_data_SS_n,
    output wire signal,
    output reg[7:0] LED
    );

parameter OOKGuard = 8'b01010101;
parameter PWMGuard = 8'b11100100;
parameter PPMGuard = 8'b11100100;
parameter DPIMGuard = 8'b00000000;

wire clk_100M, clk_10M, encode_clk, tx_ready, locked_sig;
wire sig_OOK, sig_PWM, sig_PPM, sig_DPIM, txr_OOK, txr_PWM, txr_PPM, txr_DPIM;
wire [7:0] tx_data;
wire [7:0] guard_data;
reg  [1:0] clk_select;
reg  [1:0] sig_select;
reg  [2:0] clk_div;

wire reset_tot, reset_tot_n;
assign reset_tot = ~reset_tot_n;
assign reset_tot_n = locked_sig & reset_n;

always @(posedge clk_10M or posedge reset_tot) begin
    if (reset_tot) begin
        clk_div           <= 3'd0;
        LED               <= 8'd0;
    end
    else begin
        clk_div       <= clk_div + 3'd1;
        LED           <= 8'd0;
    end
end


pll pll_inst (
    .inclk0 ( clk_12M ),
    .c0 ( clk_100M ),
    .c1 ( clk_10M ),
    .locked ( locked_sig )
    );

mux4 clk_mux (
    .data0 ( clk_10M ),
    .data1 ( clk_div[0] ),
    .data2 ( clk_div[1] ),
    .data3 ( clk_div[2] ),
    .sel ( clk_select ),
    .result ( encode_clk )
    );

mux4 sig_mux (
    .data0 ( sig_OOK ),
    .data1 ( sig_PWM ),
    .data2 ( sig_PPM ),
    .data3 ( sig_DPIM ),
    .sel ( sig_select ),
    .result ( signal )
    );

mux4 txr_mux (
    .data0 ( txr_OOK ),
    .data1 ( txr_PWM ),
    .data2 ( txr_PPM ),
    .data3 ( txr_DPIM ),
    .sel ( sig_select ),
    .result ( tx_ready )
    );

/////////////////////// START OF FLASH SPECIFIC SECTION //////////////////////////////////
    // mux2x tx_data_mux(
    //     .data0x ( flash_data_out ),
    //     .data1x ( guard_data ),
    //     .sel ( use_guard ),
    //     .result ( tx_data )
    //     );

    // mux4x guard_data_mux(
    //     .data0x ( OOKGuard ),
    //     .data1x ( PWMGuard ),
    //     .data2x ( PPMGuard ),
    //     .data3x ( DPIMGuard ),
    //     .sel ( sig_select ),
    //     .result ( guard_data )
    //     );


    // wire [7:0] state_a = 8'd0; //Idle
    // wire [7:0] state_b = 8'd1; //Send current data byte
    // wire [7:0] state_c = 8'd2; //Increment address
    // wire [7:0] state_d = 8'd3; //Wait for tx_ready (And get next spi flash data byte)
    // wire [7:0] state_e = 8'd4; //Send start guard byte
    // wire [7:0] state_f = 8'd5; //Wait start guard byte (And get first spi flash data byte)
    // wire [7:0] state_g = 8'd6; //Send end guard byte
    // wire [7:0] state_h = 8'd7; //Wait end guard byte
    // wire tx_start;
    // reg  bus_idle, tx_data_valid, use_guard, tx_active;
    // reg  [7:0] cur_tx_state;
    // reg  [23:0] flash_read_addr, MaxFlashAddr;
    // wire [23:0] flash_start_addr, flash_end_addr;


    // wire [3:0] state_idle = 4'd0;
    // wire [3:0] state_cmd  = 4'd1;
    // wire [3:0] state_addr = 4'd2;
    // wire [3:0] state_data = 4'd3;
    // reg  [3:0] cur_flash_state, cmd_counter, addr_counter, data_counter;
    // wire flash_ready;
    // reg  flash_data_valid, flash_cont_tx, read_flash_once;
    // wire [7:0] flash_data_out;
    // reg  [7:0] flash_data_in;
    // reg  [23:0] flash_read_addr_latch;

    // always @(posedge clk_100M or posedge reset_tot) begin
    //     if (reset_tot) begin
    //         cur_tx_state <= state_a;
    //         flash_read_addr <= 24'd0;
    //         bus_idle <= 1;
    //         tx_data_valid <= 0;
    //         use_guard <= 0;
    //         tx_active <= 0;

    //         read_flash_once <= 0;
    //         cur_flash_state <= state_idle;
    //         cmd_counter <= 4'd0;
    //         addr_counter <= 4'd0;
    //         data_counter <= 4'd0;

    //         flash_data_in <= 8'hff;  
    //         flash_data_valid <= 1'b0;
    //         flash_cont_tx <= 1'b0;
    //         flash_read_addr_latch <= 24'h000000;
    //     end
    //     else begin
    //         if (tx_start) begin
    //             tx_active <= 1;
    //             flash_read_addr <= flash_start_addr;
    //             MaxFlashAddr <= flash_end_addr;
    //         end
    //         else if (tx_active && (flash_read_addr >= MaxFlashAddr)) begin
    //             tx_active <= 0;
    //             flash_read_addr <= 0;
    //         end

    //     // TX Transitions
    //         if ((cur_tx_state == state_a) && tx_active) begin
    //             cur_tx_state <= state_e;
    //             read_flash_once <= 0;
    //         end
    //         else if ((cur_tx_state == state_b) && (tx_ready == 0)) begin
    //             cur_tx_state <= state_c;
    //             read_flash_once <= 0;
    //         end
    //         else if (cur_tx_state == state_c) begin
    //             cur_tx_state <= state_d;
    //         end
    //         else if ((cur_tx_state == state_d) && (tx_ready == 1)) begin
    //             if (flash_read_addr[11:0] == 0) begin
    //                 cur_tx_state <= state_g;
    //             end
    //             else begin
    //                 cur_tx_state <= state_b;
    //             end
    //         end
    //         else if ((cur_tx_state == state_e) && (tx_ready == 0)) begin
    //             cur_tx_state <= state_f;
    //         end
    //         else if ((cur_tx_state == state_f) && (tx_ready == 1)) begin
    //             cur_tx_state <= state_b;
    //         end
    //         else if ((cur_tx_state == state_g) && (tx_ready == 0)) begin
    //             cur_tx_state <= state_h;
    //         end
    //         else if ((cur_tx_state == state_h) && (tx_ready == 1)) begin
    //             cur_tx_state <= state_a;
    //         end
            
    //     // TX Actions
    //         if (cur_tx_state == state_a) begin
    //             bus_idle <= 1;
    //         end
    //         else if (cur_tx_state == state_b) begin
    //             bus_idle <= 0;
    //             tx_data_valid <= (flash_ready&(cur_flash_state == state_idle));
    //             // tx_data_valid <= 1;
    //         end
    //         else if (cur_tx_state == state_c) begin
    //             bus_idle <= 0;
    //             tx_data_valid <= 0;
    //             flash_read_addr <= flash_read_addr + 1;
    //         end
    //         else if ((cur_tx_state == state_e) || (cur_tx_state == state_g)) begin
    //             bus_idle <= 0;
    //             use_guard <= 1;
    //             tx_data_valid <= 1;
    //         end
    //         else if ((cur_tx_state == state_f) || (cur_tx_state == state_h)) begin
    //             bus_idle <= 0;
    //             use_guard <= 0;
    //             tx_data_valid <= 0;
    //         end

    //     // Flash Transitions
    //         if ((cur_flash_state == state_idle) && ((cur_tx_state == state_c) || (cur_tx_state == state_e)) &&(~read_flash_once)) begin
    //             cur_flash_state <= state_cmd;
    //         end
    //         else if ((cur_flash_state == state_cmd)&&(cmd_counter >= 1)) begin
    //             cur_flash_state <= state_addr;
    //         end
    //         else if ((cur_flash_state == state_addr) && (addr_counter >= 3)) begin
    //             cur_flash_state <= state_data;
    //         end
    //         else if ((cur_flash_state == state_data) && (data_counter >= 1)) begin
    //             cur_flash_state <= state_idle;
    //             flash_cont_tx <= 1'b0;
    //         end

    //     // Flash Actions
    //         if (cur_flash_state == state_cmd) begin
    //             read_flash_once <= 1;
    //             if (flash_ready && (~flash_data_valid)) begin
    //                 flash_data_in <= 8'h03;
    //                 flash_data_valid <= 1'b1;
    //                 flash_cont_tx <= 1'b1;
    //                 flash_read_addr_latch <= flash_read_addr;
    //             end
    //             else if (flash_data_valid) begin
    //                 cmd_counter <= cmd_counter + 4'd1;
    //                 addr_counter <= 4'd0;
    //                 data_counter <= 5'd0;
    //                 flash_data_valid <= 1'b0;
    //             end
    //         end
    //         else if (cur_flash_state == state_addr) begin
    //             if (flash_ready && (~flash_data_valid)) begin
    //                 flash_data_in <= flash_read_addr_latch[23:16];
    //                 flash_read_addr_latch = {flash_read_addr_latch[15:0], 8'h00};
    //                 flash_data_valid <= 1'b1;
    //                 flash_cont_tx <= 1'b1;
    //             end
    //             else if (flash_data_valid) begin
    //                 addr_counter <= addr_counter + 4'd1;
    //                 cmd_counter <= 4'd0;
    //                 data_counter <= 5'd0;
    //                 flash_data_valid <= 1'b0;
    //             end
    //         end
    //         else if (cur_flash_state == state_data) begin
    //             if (flash_ready && (~flash_data_valid)) begin
    //                 flash_data_in <= 8'h00;
    //                 flash_data_valid <= 1'b1;
    //                 flash_cont_tx <= 1'b1;
    //             end
    //             else if (flash_data_valid) begin
    //                 data_counter <= data_counter + 5'd1;
    //                 addr_counter <= 4'd0;
    //                 cmd_counter <= 4'd0;
    //                 flash_data_valid <= 1'b0;
    //             end
    //         end

    //     end
    // end

    // SPIMaster SPI_inst(
    //     .reset (reset_tot),
    //     .clk   (clk_100M),
    //     .data_in (flash_data_in),
    //     .data_valid (flash_data_valid),
    //     .cont_tx (flash_cont_tx),
    //     .data_out (flash_data_out),
    //     .tx_ready (flash_ready),
    //     .MISO  (vid_data_MISO),
    //     .SCLK  (vid_data_SCLK),
    //     .SS_n  (vid_data_SS_n),
    //     .MOSI  (vid_data_MOSI)
    //     );
/////////////////////// END OF FLASH SPECIFIC SECTION //////////////////////////////////
/////////////////////// START OF ROM SPECIFIC SECTION //////////////////////////////////
    wire [7:0] rom_data;
    wire tx_active = ~bus_idle;

    mux2x tx_data_mux(
        .data0x ( rom_data ),
        .data1x ( guard_data ),
        .sel ( use_guard ),
        .result ( tx_data )
        );

    mux4x guard_data_mux(
        .data0x ( OOKGuard ),
        .data1x ( PWMGuard ),
        .data2x ( PPMGuard ),
        .data3x ( DPIMGuard ),
        .sel ( sig_select ),
        .result ( guard_data )
        );

    wire [7:0] state_a = 8'd0;
    wire [7:0] state_b = 8'd1;
    wire [7:0] state_c = 8'd2;
    wire [7:0] state_d = 8'd3;
    wire [7:0] state_e = 8'd4; //Send start guard byte
    wire [7:0] state_f = 8'd5; //Wait start guard byte
    wire [7:0] state_g = 8'd6; //Send end guard byte
    wire [7:0] state_h = 8'd7; //Wait end guard byte
    wire tx_start;
    reg  bus_idle, tx_data_valid, use_guard;
    reg  [7:0] cur_state;
    reg  [11:0] ROM_address;
    wire [23:0] flash_start_addr, flash_end_addr; //So that we don't get yelled at by the compiler

    always @(posedge clk_100M or posedge reset_tot) begin
        if (reset_tot) begin
            cur_state <= state_a;
            ROM_address <= 12'd0;
            bus_idle <= 1;
            tx_data_valid <= 0;
            use_guard <= 0;
        end
        else begin
            // Transitions
            if ((cur_state == state_a) && tx_start) begin
                cur_state <= state_e;
            end
            else if ((cur_state == state_b) && (tx_ready == 0)) begin
                cur_state <= state_c;
            end
            else if (cur_state == state_c) begin
                cur_state <= state_d;
            end
            else if ((cur_state == state_d) && (tx_ready == 1)) begin
                if (ROM_address == 0) begin
                    cur_state <= state_g;
                end
                else begin
                    cur_state <= state_b;
                end
            end
            else if ((cur_state == state_e) && (tx_ready == 0)) begin
                cur_state <= state_f;
            end
            else if ((cur_state == state_f) && (tx_ready == 1)) begin
                cur_state <= state_b;
            end
            else if ((cur_state == state_g) && (tx_ready == 0)) begin
                cur_state <= state_h;
            end
            else if ((cur_state == state_h) && (tx_ready == 1)) begin
                cur_state <= state_a;
            end
            
            // Actions
            if (cur_state == state_a) begin
                bus_idle <= 1;
            end
            else if (cur_state == state_b) begin
                bus_idle <= 0;
                tx_data_valid <= 1;
            end
            else if (cur_state == state_c) begin
                bus_idle <= 0;
                tx_data_valid <= 0;
                ROM_address <= ROM_address + 1;
            end
            else if ((cur_state == state_e) || (cur_state == state_g)) begin
                bus_idle <= 0;
                use_guard <= 1;
                tx_data_valid <= 1;
            end
            else if ((cur_state == state_f) || (cur_state == state_h)) begin
                bus_idle <= 0;
                use_guard <= 0;
                tx_data_valid <= 0;
            end
        end
    end
        
    ROM ROM_inst (
        .address ( ROM_address ),
        .clock ( clk_100M ),
        .q ( rom_data )
        );
/////////////////////// END OF ROM SPECIFIC SECTION //////////////////////////////////


transmitter u0 (
    .clk_clk              (clk_100M),      //            clk.clk
    .reset_reset_n        (reset_tot_n),   //          reset.reset_n
    .tx_start_export      (tx_start),      //       tx_start.export
    .tx_ready_export      (~tx_active),    //       tx_ready.export
    .tx_clk_select_export (clk_select),    //  tx_clk_select.export
    .tx_sig_select_export (sig_select),     //  tx_sig_select.export
    .flash_start_addr_export (flash_start_addr), // flash_start_addr.export
    .flash_end_addr_export   (flash_end_addr)    //   flash_end_addr.export
    ); 


encoderOOK   encodeOOK_inst (clk_100M, encode_clk, (~sig_select[0]&~sig_select[1]), tx_data, tx_data_valid, sig_OOK,  txr_OOK);
encoder2PWM  encodePWM_inst (clk_100M, encode_clk, ( sig_select[0]&~sig_select[1]), tx_data, tx_data_valid, sig_PWM,  txr_PWM);
encoder2PPM  encodePPM_inst (clk_100M, encode_clk, (~sig_select[0]& sig_select[1]), tx_data, tx_data_valid, sig_PPM,  txr_PPM);
encoder2DPIM encodeDPIM_inst(clk_100M, encode_clk, ( sig_select[0]& sig_select[1]), tx_data, tx_data_valid, sig_DPIM, txr_DPIM);


endmodule