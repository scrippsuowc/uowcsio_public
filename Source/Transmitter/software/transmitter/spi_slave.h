/*
 * spi_slave.h
 *
 *  Created on: Aug 3, 2018
 *      Author: cgage
 */

#ifndef SPI_SLAVE_H_
#define SPI_SLAVE_H_

#include "alt_types.h"
#include "system.h"

alt_u8 spi_slave_byte_avail(void);
alt_u8 spi_slave_read_byte(void);

#endif /* SPI_SLAVE_H_ */
