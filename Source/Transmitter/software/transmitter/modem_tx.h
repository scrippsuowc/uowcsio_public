/*
 * modem_tx.h
 *
 *  Created on: Aug 3, 2018
 *      Author: cgage
 */

#ifndef MODEM_TX_H_
#define MODEM_TX_H_

#include "alt_types.h"
#include "altera_avalon_pio_regs.h"
#include "system.h"

#define NUM_CLK_SETTINGS 3

#define TX_START_BASE 0x811150
#define TX_SIG_SELECT_BASE 0x811120
#define TX_READY_BASE 0x811140
#define TX_CLK_SELECT_BASE 0x811130

#define MODEM_TX_POLL_AVAIL (IORD_ALTERA_AVALON_PIO_DATA(TX_READY_BASE))

void   modem_tx_poll_write(alt_u8 data);
void   modem_tx_set_clk_sel(alt_u8 clk_sel);
void   modem_tx_set_sig_sel(alt_u8 clk_sel);

#endif /* MODEM_TX_H_ */
