/*
 * spi_slave.c
 *
 *  Created on: Aug 3, 2018
 *      Author: cgage
 */

#include "altera_avalon_spi_regs.h"
#include "spi_slave.h"

alt_u8 spi_slave_byte_avail(void)
{
	alt_u8 status = IORD_ALTERA_AVALON_SPI_STATUS(SPI_SLAVE_BASE);
	return ((status & ALTERA_AVALON_SPI_STATUS_RRDY_MSK) == ALTERA_AVALON_SPI_STATUS_RRDY_MSK);
}

alt_u8 spi_slave_read_byte(void)
{
	return IORD_ALTERA_AVALON_SPI_RXDATA(SPI_SLAVE_BASE);
}
