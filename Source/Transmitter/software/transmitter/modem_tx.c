/*
 *  modem_tx.c
 *
 *  Created on: Aug 3, 2018
 *      Author: cgage
 */

#include <stdint.h>
#include <unistd.h>
#include "modem_tx.h"
#include "alt_types.h"
#include "altera_avalon_pio_regs.h"
#include "system.h"

//alt_u8 modem_tx_poll_avail(void)
//{
//	return IORD_ALTERA_AVALON_PIO_DATA(TX_READY_BASE);
//}

void modem_tx_poll_write(alt_u8 data)
{
//	  IOWR_ALTERA_AVALON_PIO_DATA(TX_DATA_BASE, data);
	  IOWR_ALTERA_AVALON_PIO_DATA(TX_START_BASE, 1);
	  IOWR_ALTERA_AVALON_PIO_DATA(TX_START_BASE, 0);
}

void   modem_tx_set_clk_sel(alt_u8 clk_sel)
{
	  IOWR_ALTERA_AVALON_PIO_DATA(TX_CLK_SELECT_BASE, clk_sel);
}

void   modem_tx_set_sig_sel(alt_u8 clk_sel)
{
	  IOWR_ALTERA_AVALON_PIO_DATA(TX_SIG_SELECT_BASE, clk_sel);
}
