/*
 *
 *
 */

#include <stdint.h>
#include <unistd.h>
#include "altera_avalon_pio_regs.h"
#include "modem_tx.h"
#include "alt_types.h"
#include "system.h"

//#define TX_IMG
//#define TX_VID

#define NUM_IMAGE_BYTES 1672192
#define NUM_VIDEO_BYTES 2319104

#ifdef TX_IMG
#define NUM_TX_BYTES NUM_IMAGE_BYTES
#else
#define NUM_TX_BYTES NUM_VIDEO_BYTES
#endif

#define FLASH_START_ADDR_BASE 0x811180
#define FLASH_END_ADDR_BASE 0x811190

#if defined(TX_VID) || defined(TX_IMG)
#define NUM_ENCODINGS 1
const alt_u32 FourKChunks = 65;
#else
#define NUM_ENCODINGS 4
#endif


int main()
{
	while(1) {
		for (alt_u8 encoding = 0; encoding < NUM_ENCODINGS; encoding++) {
			modem_tx_set_sig_sel(encoding);
#if defined(TX_VID) || defined(TX_IMG)
			modem_tx_set_clk_sel(1);
			alt_u32 startAddr=0;
			alt_u32 numBytesPerChunk = (4096*FourKChunks);
			for (; startAddr < NUM_TX_BYTES; startAddr+=numBytesPerChunk){
				IOWR_ALTERA_AVALON_PIO_DATA(FLASH_START_ADDR_BASE, startAddr);
				IOWR_ALTERA_AVALON_PIO_DATA(FLASH_END_ADDR_BASE, startAddr+numBytesPerChunk);
#else
			for (alt_u8 clock_div = 0; clock_div < NUM_CLK_SETTINGS; clock_div++) {
				modem_tx_set_clk_sel(clock_div);
#endif
				modem_tx_poll_write(0x00);
				while (!MODEM_TX_POLL_AVAIL); //Wait for last transmission to end
			}
		}
	}

	return 0;
}
