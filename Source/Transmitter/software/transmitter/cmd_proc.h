/*
 * cmd_proc.h
 *
 *  Created on: Aug 7, 2018
 *      Author: Chris
 */

#ifndef CMD_PROC_H_
#define CMD_PROC_H_

#include "alt_types.h"

#define CMD_PROC_STATE_WAIT 0
#define CMD_PROC_STATE_LEN  1
#define CMD_PROC_STATE_DATA 2

void cmd_proc_init(void);
void cmd_proc_process_byte(alt_u8 data);
alt_u8 cmd_proc_tx_data_avail(void);
alt_u8 cmd_proc_tx_data_get(void);


#endif /* CMD_PROC_H_ */
