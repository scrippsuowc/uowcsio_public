/*
 * test_pattern.h
 *
 *  Created on: Aug 9, 2018
 *      Author: Chris
 */

#ifndef TEST_PATTERN_H_
#define TEST_PATTERN_H_

#include "alt_types.h"

#define TEST_PATTERN_LEN  4096
#define NUM_TEST_PATTERNS 2

#define ROW_1(data)    data, data, data, data, data, data, data, data, data, data, data, data, data, data, data, data
#define ROWS_8(data)   ROW_1(data),    ROW_1(data+1), ROW_1(data+2), ROW_1(data+3), ROW_1(data+4), ROW_1(data+5), ROW_1(data+6), ROW_1(data+7)
#define ROWS_16(data)  ROWS_8(data),   ROWS_8(data + 8)
#define ROWS_32(data)  ROWS_16(data),  ROWS_16(data + 16)
#define ROWS_64(data)  ROWS_32(data),  ROWS_32(data + 32)
#define ROWS_128(data) ROWS_64(data),  ROWS_64(data + 64)
#define ROWS_256(data) ROWS_128(data), ROWS_128(data + 128)

alt_u8 test_pattern_0[TEST_PATTERN_LEN] = {
    ROWS_256(0)
};

//Create 16 sets of consecutive bytes ranging from 0 to 255
#define CONS_8(data)   data, data+1, data+2, data+3, data+4, data+5, data+6, data+7
#define CONS_16(data)  CONS_8(data),   CONS_8(data+8)
#define CONS_32(data)  CONS_16(data),  CONS_16(data+16)
#define CONS_64(data)  CONS_32(data),  CONS_32(data+32)
#define CONS_128(data) CONS_64(data),  CONS_64(data+64)
#define CONS_256(data) CONS_128(data), CONS_128(data+128)

alt_u8 test_pattern_1[TEST_PATTERN_LEN] = {
	CONS_256(0), CONS_256(0), CONS_256(0), CONS_256(0), CONS_256(0), CONS_256(0), CONS_256(0), CONS_256(0),
	CONS_256(0), CONS_256(0), CONS_256(0), CONS_256(0), CONS_256(0), CONS_256(0), CONS_256(0), CONS_256(0)
};

alt_u8 * test_pattern_array[NUM_TEST_PATTERNS] = {
		test_pattern_0,
		test_pattern_1
};

#endif /* TEST_PATTERN_H_ */
