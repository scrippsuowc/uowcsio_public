/*
 * cmd_proc.c
 *
 *  Created on: Aug 7, 2018
 *      Author: Chris
 */

#include <stdio.h>
#include "cmd_proc.h"
#define CIRC_BUFF_SIZE 2048
#include "CircBuff.h"
#include "UOWC_SPI_cmds.h"
#include "modem_tx.h"

circBuff_t tx_cb;

void cmd_proc_init(void)
{
	circBuffInit(&tx_cb);
}

void cmd_proc_process_byte(alt_u8 data)
{
	static alt_u8 state = CMD_PROC_STATE_WAIT;
	static alt_u8 data_len = 0;

	if (CMD_PROC_STATE_WAIT == state) {
		switch(data) {
			case UOWC_SPI_CMD_TX:
				state = CMD_PROC_STATE_LEN;
				return;
			case UOWC_SPI_CMD_CLK_SEL:
				modem_tx_set_clk_sel(data);
//				printf("Setting clock to %f MHz\n", (10/(float)(data+1)));
			default:
				return;
		}
	} else if (CMD_PROC_STATE_LEN == state) {
		data_len = data;
		state = CMD_PROC_STATE_DATA;
	} else if (CMD_PROC_STATE_DATA == state) {
		if (data_len) {
			circBuffPut(&tx_cb, data);
			data_len--;
		}
		if (0 == data_len) {
			state = CMD_PROC_STATE_WAIT;
		}
	} else {
		state = CMD_PROC_STATE_WAIT;
		data_len = 0;
	}
}

alt_u8 cmd_proc_tx_data_avail(void)
{
	return !circBuffEmpty(&tx_cb);
}

alt_u8 cmd_proc_tx_data_get(void)
{
	alt_u8 Val = 0;
	circBuffGet(&tx_cb, &Val);
	return Val;
}
