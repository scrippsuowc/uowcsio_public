/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'transmitter_cpu' in SOPC Builder design 'transmitter'
 * SOPC Builder design path: ../../transmitter.sopcinfo
 *
 * Generated: Fri Aug 17 11:06:02 PDT 2018
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_gen2"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x00010820
#define ALT_CPU_CPU_ARCH_NIOS2_R1
#define ALT_CPU_CPU_FREQ 100000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000000
#define ALT_CPU_CPU_IMPLEMENTATION "tiny"
#define ALT_CPU_DATA_ADDR_WIDTH 0x11
#define ALT_CPU_DCACHE_LINE_SIZE 0
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_DCACHE_SIZE 0
#define ALT_CPU_EXCEPTION_ADDR 0x00008020
#define ALT_CPU_FLASH_ACCELERATOR_LINES 0
#define ALT_CPU_FLASH_ACCELERATOR_LINE_SIZE 0
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 100000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 0
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 0
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 0
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_ICACHE_SIZE 0
#define ALT_CPU_INST_ADDR_WIDTH 0x11
#define ALT_CPU_NAME "transmitter_cpu"
#define ALT_CPU_OCI_VERSION 1
#define ALT_CPU_RESET_ADDR 0x00008000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x00010820
#define NIOS2_CPU_ARCH_NIOS2_R1
#define NIOS2_CPU_FREQ 100000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000000
#define NIOS2_CPU_IMPLEMENTATION "tiny"
#define NIOS2_DATA_ADDR_WIDTH 0x11
#define NIOS2_DCACHE_LINE_SIZE 0
#define NIOS2_DCACHE_LINE_SIZE_LOG2 0
#define NIOS2_DCACHE_SIZE 0
#define NIOS2_EXCEPTION_ADDR 0x00008020
#define NIOS2_FLASH_ACCELERATOR_LINES 0
#define NIOS2_FLASH_ACCELERATOR_LINE_SIZE 0
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 0
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 0
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 0
#define NIOS2_ICACHE_LINE_SIZE_LOG2 0
#define NIOS2_ICACHE_SIZE 0
#define NIOS2_INST_ADDR_WIDTH 0x11
#define NIOS2_OCI_VERSION 1
#define NIOS2_RESET_ADDR 0x00008000


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_SPI
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_NIOS2_GEN2


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "MAX 10"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/jtag_uart"
#define ALT_STDERR_BASE 0x11078
#define ALT_STDERR_DEV jtag_uart
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/jtag_uart"
#define ALT_STDIN_BASE 0x11078
#define ALT_STDIN_DEV jtag_uart
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/jtag_uart"
#define ALT_STDOUT_BASE 0x11078
#define ALT_STDOUT_DEV jtag_uart
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "transmitter"


/*
 * hal configuration
 *
 */

#define ALT_INCLUDE_INSTRUCTION_RELATED_EXCEPTION_API
#define ALT_MAX_FD 32
#define ALT_SYS_CLK none
#define ALT_TIMESTAMP_CLK none


/*
 * jtag_uart configuration
 *
 */

#define ALT_MODULE_CLASS_jtag_uart altera_avalon_jtag_uart
#define JTAG_UART_BASE 0x11078
#define JTAG_UART_IRQ 2
#define JTAG_UART_IRQ_INTERRUPT_CONTROLLER_ID 0
#define JTAG_UART_NAME "/dev/jtag_uart"
#define JTAG_UART_READ_DEPTH 64
#define JTAG_UART_READ_THRESHOLD 8
#define JTAG_UART_SPAN 8
#define JTAG_UART_TYPE "altera_avalon_jtag_uart"
#define JTAG_UART_WRITE_DEPTH 64
#define JTAG_UART_WRITE_THRESHOLD 8


/*
 * onchip_mem configuration
 *
 */

#define ALT_MODULE_CLASS_onchip_mem altera_avalon_onchip_memory2
#define ONCHIP_MEM_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define ONCHIP_MEM_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define ONCHIP_MEM_BASE 0x8000
#define ONCHIP_MEM_CONTENTS_INFO ""
#define ONCHIP_MEM_DUAL_PORT 0
#define ONCHIP_MEM_GUI_RAM_BLOCK_TYPE "AUTO"
#define ONCHIP_MEM_INIT_CONTENTS_FILE "transmitter_onchip_mem"
#define ONCHIP_MEM_INIT_MEM_CONTENT 0
#define ONCHIP_MEM_INSTANCE_ID "NONE"
#define ONCHIP_MEM_IRQ -1
#define ONCHIP_MEM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ONCHIP_MEM_NAME "/dev/onchip_mem"
#define ONCHIP_MEM_NON_DEFAULT_INIT_FILE_ENABLED 0
#define ONCHIP_MEM_RAM_BLOCK_TYPE "AUTO"
#define ONCHIP_MEM_READ_DURING_WRITE_MODE "DONT_CARE"
#define ONCHIP_MEM_SINGLE_CLOCK_OP 0
#define ONCHIP_MEM_SIZE_MULTIPLE 1
#define ONCHIP_MEM_SIZE_VALUE 32768
#define ONCHIP_MEM_SPAN 32768
#define ONCHIP_MEM_TYPE "altera_avalon_onchip_memory2"
#define ONCHIP_MEM_WRITABLE 1


/*
 * spi_slave configuration
 *
 */

#define ALT_MODULE_CLASS_spi_slave altera_avalon_spi
#define SPI_SLAVE_BASE 0x11000
#define SPI_SLAVE_CLOCKMULT 1
#define SPI_SLAVE_CLOCKPHASE 0
#define SPI_SLAVE_CLOCKPOLARITY 0
#define SPI_SLAVE_CLOCKUNITS "Hz"
#define SPI_SLAVE_DATABITS 8
#define SPI_SLAVE_DATAWIDTH 16
#define SPI_SLAVE_DELAYMULT "1.0E-9"
#define SPI_SLAVE_DELAYUNITS "ns"
#define SPI_SLAVE_EXTRADELAY 0
#define SPI_SLAVE_INSERT_SYNC 0
#define SPI_SLAVE_IRQ 1
#define SPI_SLAVE_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_SLAVE_ISMASTER 0
#define SPI_SLAVE_LSBFIRST 0
#define SPI_SLAVE_NAME "/dev/spi_slave"
#define SPI_SLAVE_NUMSLAVES 1
#define SPI_SLAVE_PREFIX "spi_"
#define SPI_SLAVE_SPAN 32
#define SPI_SLAVE_SYNC_REG_DEPTH 2
#define SPI_SLAVE_TARGETCLOCK 10000000u
#define SPI_SLAVE_TARGETSSDELAY "0.0"
#define SPI_SLAVE_TYPE "altera_avalon_spi"


/*
 * sysid configuration
 *
 */

#define ALT_MODULE_CLASS_sysid altera_avalon_sysid_qsys
#define SYSID_BASE 0x11070
#define SYSID_ID 1431262296
#define SYSID_IRQ -1
#define SYSID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SYSID_NAME "/dev/sysid"
#define SYSID_SPAN 8
#define SYSID_TIMESTAMP 1534526697
#define SYSID_TYPE "altera_avalon_sysid_qsys"


/*
 * tx_clk_select configuration
 *
 */

#define ALT_MODULE_CLASS_tx_clk_select altera_avalon_pio
#define TX_CLK_SELECT_BASE 0x11030
#define TX_CLK_SELECT_BIT_CLEARING_EDGE_REGISTER 0
#define TX_CLK_SELECT_BIT_MODIFYING_OUTPUT_REGISTER 0
#define TX_CLK_SELECT_CAPTURE 0
#define TX_CLK_SELECT_DATA_WIDTH 2
#define TX_CLK_SELECT_DO_TEST_BENCH_WIRING 0
#define TX_CLK_SELECT_DRIVEN_SIM_VALUE 0
#define TX_CLK_SELECT_EDGE_TYPE "NONE"
#define TX_CLK_SELECT_FREQ 100000000
#define TX_CLK_SELECT_HAS_IN 0
#define TX_CLK_SELECT_HAS_OUT 1
#define TX_CLK_SELECT_HAS_TRI 0
#define TX_CLK_SELECT_IRQ -1
#define TX_CLK_SELECT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define TX_CLK_SELECT_IRQ_TYPE "NONE"
#define TX_CLK_SELECT_NAME "/dev/tx_clk_select"
#define TX_CLK_SELECT_RESET_VALUE 0
#define TX_CLK_SELECT_SPAN 16
#define TX_CLK_SELECT_TYPE "altera_avalon_pio"


/*
 * tx_data configuration
 *
 */

#define ALT_MODULE_CLASS_tx_data altera_avalon_pio
#define TX_DATA_BASE 0x11060
#define TX_DATA_BIT_CLEARING_EDGE_REGISTER 0
#define TX_DATA_BIT_MODIFYING_OUTPUT_REGISTER 0
#define TX_DATA_CAPTURE 0
#define TX_DATA_DATA_WIDTH 8
#define TX_DATA_DO_TEST_BENCH_WIRING 0
#define TX_DATA_DRIVEN_SIM_VALUE 0
#define TX_DATA_EDGE_TYPE "NONE"
#define TX_DATA_FREQ 100000000
#define TX_DATA_HAS_IN 0
#define TX_DATA_HAS_OUT 1
#define TX_DATA_HAS_TRI 0
#define TX_DATA_IRQ -1
#define TX_DATA_IRQ_INTERRUPT_CONTROLLER_ID -1
#define TX_DATA_IRQ_TYPE "NONE"
#define TX_DATA_NAME "/dev/tx_data"
#define TX_DATA_RESET_VALUE 0
#define TX_DATA_SPAN 16
#define TX_DATA_TYPE "altera_avalon_pio"


/*
 * tx_data_valid configuration
 *
 */

#define ALT_MODULE_CLASS_tx_data_valid altera_avalon_pio
#define TX_DATA_VALID_BASE 0x11050
#define TX_DATA_VALID_BIT_CLEARING_EDGE_REGISTER 0
#define TX_DATA_VALID_BIT_MODIFYING_OUTPUT_REGISTER 0
#define TX_DATA_VALID_CAPTURE 0
#define TX_DATA_VALID_DATA_WIDTH 1
#define TX_DATA_VALID_DO_TEST_BENCH_WIRING 0
#define TX_DATA_VALID_DRIVEN_SIM_VALUE 0
#define TX_DATA_VALID_EDGE_TYPE "NONE"
#define TX_DATA_VALID_FREQ 100000000
#define TX_DATA_VALID_HAS_IN 0
#define TX_DATA_VALID_HAS_OUT 1
#define TX_DATA_VALID_HAS_TRI 0
#define TX_DATA_VALID_IRQ -1
#define TX_DATA_VALID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define TX_DATA_VALID_IRQ_TYPE "NONE"
#define TX_DATA_VALID_NAME "/dev/tx_data_valid"
#define TX_DATA_VALID_RESET_VALUE 0
#define TX_DATA_VALID_SPAN 16
#define TX_DATA_VALID_TYPE "altera_avalon_pio"


/*
 * tx_ready configuration
 *
 */

#define ALT_MODULE_CLASS_tx_ready altera_avalon_pio
#define TX_READY_BASE 0x11040
#define TX_READY_BIT_CLEARING_EDGE_REGISTER 0
#define TX_READY_BIT_MODIFYING_OUTPUT_REGISTER 0
#define TX_READY_CAPTURE 1
#define TX_READY_DATA_WIDTH 1
#define TX_READY_DO_TEST_BENCH_WIRING 0
#define TX_READY_DRIVEN_SIM_VALUE 0
#define TX_READY_EDGE_TYPE "RISING"
#define TX_READY_FREQ 100000000
#define TX_READY_HAS_IN 1
#define TX_READY_HAS_OUT 0
#define TX_READY_HAS_TRI 0
#define TX_READY_IRQ 0
#define TX_READY_IRQ_INTERRUPT_CONTROLLER_ID 0
#define TX_READY_IRQ_TYPE "EDGE"
#define TX_READY_NAME "/dev/tx_ready"
#define TX_READY_RESET_VALUE 0
#define TX_READY_SPAN 16
#define TX_READY_TYPE "altera_avalon_pio"


/*
 * tx_sig_select configuration
 *
 */

#define ALT_MODULE_CLASS_tx_sig_select altera_avalon_pio
#define TX_SIG_SELECT_BASE 0x11020
#define TX_SIG_SELECT_BIT_CLEARING_EDGE_REGISTER 0
#define TX_SIG_SELECT_BIT_MODIFYING_OUTPUT_REGISTER 0
#define TX_SIG_SELECT_CAPTURE 0
#define TX_SIG_SELECT_DATA_WIDTH 2
#define TX_SIG_SELECT_DO_TEST_BENCH_WIRING 0
#define TX_SIG_SELECT_DRIVEN_SIM_VALUE 0
#define TX_SIG_SELECT_EDGE_TYPE "NONE"
#define TX_SIG_SELECT_FREQ 100000000
#define TX_SIG_SELECT_HAS_IN 0
#define TX_SIG_SELECT_HAS_OUT 1
#define TX_SIG_SELECT_HAS_TRI 0
#define TX_SIG_SELECT_IRQ -1
#define TX_SIG_SELECT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define TX_SIG_SELECT_IRQ_TYPE "NONE"
#define TX_SIG_SELECT_NAME "/dev/tx_sig_select"
#define TX_SIG_SELECT_RESET_VALUE 0
#define TX_SIG_SELECT_SPAN 16
#define TX_SIG_SELECT_TYPE "altera_avalon_pio"

#endif /* __SYSTEM_H_ */
