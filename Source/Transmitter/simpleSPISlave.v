module simpleSPISlave(
input wire reset,
input wire SCLK,
input wire MOSI,

output reg [7:0] data_out,
output reg data_valid
);

reg [3:0] bitCounter;

always @(posedge SCLK or posedge reset) begin
    if (reset) begin
        data_out <= 0;
        data_valid <= 0;
        bitCounter <= 4'd0;
    end
    else begin
        data_out <= {data_out[6:0], MOSI};
        if (bitCounter == 4'd7) begin
            data_valid <= 1;
            bitCounter <= 0;
        end
        else begin
            data_valid <= 0;
            bitCounter <= bitCounter + 4'd1;
        end
    end
end

endmodule