module top (
	input  wire clk_12M,
	input  wire reset_n,
	output reg signal,
	output reg[7:0] LED
);

reg [31:0] oneseccounter;
wire reset_tot, clk_100M;
assign reset_tot = ~reset_n;

pll	pll_inst (
	.inclk0 ( clk_12M ),
	.c0 ( clk_100M )
	);

always @(posedge clk_100M or posedge reset_tot) begin
	if (reset_tot) begin
		oneseccounter <= 32'd0;
		signal <= 0;
	end
	else begin
		LED 		  <= 8'd0;
		oneseccounter <= oneseccounter + 32'd1;
		// if (oneseccounter == 32'd100000000) begin //1000 ms pulse
		// if (oneseccounter == 32'd10000000) begin  // 100 ms pulse
		// if (oneseccounter == 32'd1000000) begin   //  10 ms pulse
		// if (oneseccounter == 32'd100000) begin    //   1 ms pulse
		// if (oneseccounter == 32'd10000) begin     // 100 us pulse
		// if (oneseccounter == 32'd1000) begin      //  10 us pulse
		// if (oneseccounter == 32'd100) begin       //   1 us pulse
		// if (oneseccounter == 32'd40) begin        // 400 ns pulse
		// if (oneseccounter == 32'd20) begin        // 200 ns pulse
		if (oneseccounter == 32'd10) begin        // 100 ns pulse
		// if (oneseccounter == 32'd5) begin        //  50 ns pulse
		// if (oneseccounter == 32'd1) begin        //  10 ns pulse
			signal <= ~signal;
			oneseccounter <= 0;
		end
	end
end

endmodule